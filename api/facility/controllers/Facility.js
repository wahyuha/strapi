'use strict';

/**
 * Facility.js controller
 *
 * @description: A set of functions called "actions" for managing `Facility`.
 */

module.exports = {

  /**
   * Retrieve facility records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.facility.search(ctx.query);
    } else {
      return strapi.services.facility.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a facility record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.facility.fetch(ctx.params);
  },

  /**
   * Count facility records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.facility.count(ctx.query);
  },

  /**
   * Create a/an facility record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.facility.add(ctx.request.body);
  },

  /**
   * Update a/an facility record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.facility.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an facility record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.facility.remove(ctx.params);
  },

  /**
   * Add relation to a/an facility record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.facility.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an facility record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.facility.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an facility record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.facility.removeRelation(ctx.params, ctx.request.body);
  }
};
