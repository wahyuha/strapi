'use strict';

/**
 * University.js controller
 *
 * @description: A set of functions called "actions" for managing `University`.
 */

module.exports = {

  /**
   * Retrieve university records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.university.search(ctx.query);
    } else {
      return strapi.services.university.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a university record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.university.fetch(ctx.params);
  },

  /**
   * Count university records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.university.count(ctx.query);
  },

  /**
   * Create a/an university record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.university.add(ctx.request.body);
  },

  /**
   * Update a/an university record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.university.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an university record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.university.remove(ctx.params);
  },

  /**
   * Add relation to a/an university record.
   *
   * @return {Object}
   */

  createRelation: async (ctx, next) => {
    return strapi.services.university.addRelation(ctx.params, ctx.request.body);
  },

  /**
   * Update relation to a/an university record.
   *
   * @return {Object}
   */

  updateRelation: async (ctx, next) => {
    return strapi.services.university.editRelation(ctx.params, ctx.request.body);
  },

  /**
   * Destroy relation to a/an university record.
   *
   * @return {Object}
   */

  destroyRelation: async (ctx, next) => {
    return strapi.services.university.removeRelation(ctx.params, ctx.request.body);
  }
};
